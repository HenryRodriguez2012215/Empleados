﻿Public Class Form1
    Private Sub Form1_FontChanged(sender As Object, e As EventArgs) Handles Me.FontChanged
        conn.Close()
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        conexionAccess()
        Label1.Text = estado
    End Sub

    Private Sub btnEnviar_Click(sender As Object, e As EventArgs) Handles btnEnviar.Click

        Dim exepcion As Boolean = False

        Try

            comando = New OleDb.OleDbCommand("INSERT INTO trabajadores(DPI, Nombres, Apellidos, Departamento)" & Chr(13) &
                                         "VALUES(txtDPI, txtNombre, txtApellido, txtDepartamento)", conn)
            comando.Parameters.AddWithValue("@DPI", txtDPI.Text)
            comando.Parameters.AddWithValue("@Nombres", txtNombre.Text)
            comando.Parameters.AddWithValue("@Apellidos", txtApellido.Text)
            comando.Parameters.AddWithValue("@Departamento", txtDepartamento.Text)
            comando.ExecuteNonQuery()

        Catch ex As Exception

            exepcion = True
            MsgBox("El registro no se almaceno", MsgBoxStyle.Information, "Nuevo Registro")
            MsgBox(ex.ToString)

        End Try

        If exepcion = False Then
            MsgBox("Registro almacenado correctamente", MsgBoxStyle.Information, "Nuevo Registro")
        End If


    End Sub
End Class
